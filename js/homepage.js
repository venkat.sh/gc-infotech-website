function logoTamilVersion() {
  document.getElementById("navMenu-home").classList.add("mystyle");
  document.getElementById("navMenu-aboutGcc").classList.add("mystyle");
  document.getElementById("navMenu-whoIsWho").classList.add("mystyle");
  document.getElementById("navMenu-council").classList.add("mystyle");
  document.getElementById("navMenu-tender").classList.add("mystyle");
  document.getElementById("navMenu-departments").classList.add("mystyle");
  document.getElementById("navMenu-services").classList.add("mystyle");
  document.getElementById("navbar-main").classList.add("margin-left");
  document.getElementById("navbar-ul").classList.add("margin-left");
  document.getElementById("navbar").classList.add("padding-left");
  document.getElementById("galleryTitle-gallery").classList.add("font-sizee");
  document.getElementById("tamilContent-font").classList.add("footer-law-font");
  document.getElementById("footerTitle-law").classList.add("footer-law-font");
  document.getElementById("banner-newsletter").classList.add("footer-law-font");
  document.getElementById("whatsNewTitle-line1").classList.add("font-sizee");
  document.getElementById("whatsNewTitle-line2").classList.add("font-sizee");
  document.getElementById("whatsNewTitle-line3").classList.add("font-sizee");
  document.getElementById("whatsNewTitle-line4").classList.add("font-sizee");
  document.getElementById("whatsNewTitle-line5").classList.add("font-sizee");
  document.getElementById("whatsNewTitle-line6").classList.add("font-sizee");
}

function removeClass() {
  document.getElementById("navMenu-home").classList.remove("mystyle");
  document.getElementById("navMenu-aboutGcc").classList.remove("mystyle");
  document.getElementById("navMenu-whoIsWho").classList.remove("mystyle");
  document.getElementById("navMenu-council").classList.remove("mystyle");
  document.getElementById("navMenu-tender").classList.remove("mystyle");
  document.getElementById("navMenu-departments").classList.remove("mystyle");
  document.getElementById("navMenu-services").classList.remove("mystyle");
  document.getElementById("navbar-main").classList.remove("margin-left");
  document.getElementById("navbar-ul").classList.remove("margin-left");
  document.getElementById("navbar").classList.remove("padding-left");
  document.getElementById("galleryTitle-gallery").classList.remove("font-sizee");
  document.getElementById("tamilContent-font").classList.remove("footer-law-font");
  document.getElementById("footerTitle-law").classList.remove("footer-law-font");
  document.getElementById("banner-newsletter").classList.remove("footer-law-font");
  document.getElementById("whatsNewTitle-line1").classList.remove("font-sizee");
  document.getElementById("whatsNewTitle-line2").classList.remove("font-sizee");
  document.getElementById("whatsNewTitle-line3").classList.remove("font-sizee");
  document.getElementById("whatsNewTitle-line4").classList.remove("font-sizee");
  document.getElementById("whatsNewTitle-line5").classList.remove("font-sizee");
  document.getElementById("whatsNewTitle-line6").classList.remove("font-sizee");

}


function myFunction() {
  var x = document.getElementById("demo");
  if (window.matchMedia("(max-width: 700px)").matches) {
    x.innerHTML = "The screen is less than, or equal to, 700 pixels wide.";
  } else {
    x.innerHTML = "The screen is at least 700 pixels wide.";
  }
}


// Selecting button element
//var tamilFont = document.getElementById("tamilFont");

// Assigning event listeners to the button
// tamilFont.addEventListener("click", logoTamilVersion);
// tamilFont.addEventListener("click", toggleLanguage);


// var englishFont = document.getElementById("englishFont");

// englishFont.addEventListener("click", toggleLanguage);
// englishFont.addEventListener("click", removeClass);




