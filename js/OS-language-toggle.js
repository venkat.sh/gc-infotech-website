var defaultLanguage = 'english';
var websiteContent = {
  "english": {
    "topBar": {
      "feedback": 'Feedback',
      "complaintNumber": 'COMPLAINT NUMBER - 1913',
      "siteMap": 'Site map',
      "login": 'Login'
    },
    "navMenu": {
      "home":'Home',
      "aboutGcc":'About GCC',
      "whoIsWho":'Who is Who',
      "council":'Council',
      "tender":'Tender',
      "departments": 'Departments',
      "services": 'Online Services'
    },
    "OSTitle":{
      "title":'ONLINE SERVICES'
    },
    "breadCrumb":{
      "OS":'Online Services'
    },
    "OSTile":{
      "birthCertificate":'Birth Certificate',
      "deathCertificate":'Death Certificate',
      "childNameInclusion":'Child Name Inclusion',
      "propertyTax":'Property Tax',
      "professionTax":'Profession Tax',
      "townPlanning":'Town Planning',
      "tradeLicenseSchedule":'Trade License Schedule',
      "onlineTaxPayment":'Online Tax Payment',
      "comminityHall":'Community Hall'
    },
    "footerTitle": {
      "home":'Home',
      "aboutGCC":'About GCC',
      "departments":'Departments',
      "complaints":'Complaints',
      "weeklyBulletin":'Weekly Bulletin',
      "licensedSurveyors":'Licensed Surveyors',
      "architectlist":'Architect list',
      "siteMap":'Site Map',
      "law":'"The Bribery is against the Law. The Complaints about corruption may be sent to the Directorate of vigilance and Anti-Corruption, Chennai-16"',
      "website":'Website: www.dvac.tn.gov.in',
      "phonenumber":'Phone No. 22310989 / 22321090 / 223211085'
    }
  },
  "tamil": {
    "topBar": {
      "feedback": 'பின்னூட்டம்',
      "complaintNumber": 'புகார் எண் – 1913',
      "siteMap": 'தள வரைபடம்',
      "login": 'உள்நுழைய'
    },
    "navMenu": {
      "home": 'முகப்பு',
      "aboutGcc": 'சென்னை மாநகராட்சியைப் பற்றி',
      "whoIsWho": 'நிர்வாகம்',
      "council": 'மன்றம்',
      "tender": 'ஒப்பந்தப்புள்ளிகள்',
      "departments": 'துறைகள்',
      "services": 'இணையதள சேவைகள்'
    },
    "OSTitle":{
      "title":'இணையதள சேவைகள்'
    },
    "breadCrumb":{
      "OS":'இணையதள சேவைகள்'
    },
    "OSTile":{
      "birthCertificate":'பிறப்பு சான்றிதழ்',
      "deathCertificate":'இறப்பு சான்றிதழ்',
      "childNameInclusion":'குழந்தை பெயர் சேர்த்தல்',
      "propertyTax":'சொத்து வரி',
      "professionTax":'தொழில் வரி',
      "townPlanning":'நகரமைப்பு திட்டம்',
      "tradeLicenseSchedule":'வர்த்தக உரிமம் அட்டவணை',
      "onlineTaxPayment":'ஆன்லைன் வரி செலுத்துதல்',
      "comminityHall":'சமூக கூடம்'
    },
    "footerTitle": {
      "home":'முகப்பு',
      "aboutGCC":'சென்னை மாநகராட்சியைப் பற்றி',
      "departments":'துறைகள்',
      "complaints":'புகார்கள்',
      "weeklyBulletin":'வாராந்திர அறிவிப்புகள்',
      "licensedSurveyors":'உரிமம் பெற்ற நில அளவர்கள்',
      "architectlist":'கட்டிடக்கலை நிபுணர்கள் பட்டியல்',
      "siteMap":'தள வரைபடம்',
      "law":'“லஞ்சம் சட்டத்திற்கு எதிரானது. ஊழல் தொடர்பான புகார்கள் சென்னை - 16 கண்காணிப்பு மற்றும் ஊழல் தடுப்பு இயக்குநரகத்திற்கு அனுப்பப்படலாம்."',
      "website":'இணையதளம்: www.dvac.tn.gov.in',
      "phonenumber":'தொலைபேசி எண்கள் - 22310989 / 22321090 / 223211085'
    }
  }
};

var contentKeys = [
  "topBar-feedback",
  "topBar-complaintNumber",
  "topBar-siteMap",
  "topBar-login",
  "navMenu-home",
  "navMenu-aboutGcc",
  "navMenu-whoIsWho",
  "navMenu-council",
  "navMenu-tender",
  "navMenu-departments",
  "navMenu-services",
  "OSTitle-title",
  "breadCrumb-OS",
  "OSTile-birthCertificate",
  "OSTile-deathCertificate",
  "OSTile-childNameInclusion",
  "OSTile-propertyTax",
  "OSTile-professionTax",
  "OSTile-townPlanning",
  "OSTile-tradeLicenseSchedule",
  "OSTile-onlineTaxPayment",
  "OSTile-comminityHall",
  "footerTitle-home",
  "footerTitle-aboutGCC",
  "footerTitle-departments",
  "footerTitle-complaints",
  "footerTitle-weeklyBulletin",
  "footerTitle-licensedSurveyors",
  "footerTitle-architectlist",
  "footerTitle-siteMap",
  "footerTitle-law",
  "footerTitle-website",
  "footerTitle-phonenumber"
]







