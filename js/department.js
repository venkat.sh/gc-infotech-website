function logoTamilVersion() {
  ///////////////////////////////////////// navbar chnages
  document.getElementById("navbar").classList.add("padding-left");
  document.getElementById("navbar-main").classList.add("margin-left");
  document.getElementById("navbar-ul").classList.add("margin-left");
  document.getElementById("navMenu-home").classList.add("mystyle");
  document.getElementById("navMenu-aboutGcc").classList.add("mystyle");
  document.getElementById("navMenu-whoIsWho").classList.add("mystyle");
  document.getElementById("navMenu-council").classList.add("mystyle");
  document.getElementById("navMenu-tender").classList.add("mystyle");
  document.getElementById("navMenu-departments").classList.add("mystyle");
  document.getElementById("navMenu-services").classList.add("mystyle");
  //////////////////// footer changes
  document.getElementById("tamilContent-font").classList.add("footer-law-font");
  document.getElementById("footerTitle-law").classList.add("footer-law-font");
}

function removeClass() {
  ///////////////////////////////////////// navbar chnages
  document.getElementById("navbar").classList.remove("padding-left");
  document.getElementById("navbar-main").classList.remove("margin-left");
  document.getElementById("navbar-ul").classList.remove("margin-left");
  document.getElementById("navMenu-home").classList.remove("mystyle");
  document.getElementById("navMenu-aboutGcc").classList.remove("mystyle");
  document.getElementById("navMenu-whoIsWho").classList.remove("mystyle");
  document.getElementById("navMenu-council").classList.remove("mystyle");
  document.getElementById("navMenu-tender").classList.remove("mystyle");
  document.getElementById("navMenu-departments").classList.remove("mystyle");
  document.getElementById("navMenu-services").classList.remove("mystyle");
  //////////////////// footer changes
  document.getElementById("tamilContent-font").classList.remove("footer-law-font");
  document.getElementById("footerTitle-law").classList.remove("footer-law-font");
}

// Selecting button element
var tamilFont = document.getElementById("tamilFont");

// Assigning event listeners to the button
tamilFont.addEventListener("click", logoTamilVersion);
tamilFont.addEventListener("click", toggleLanguage);

var englishFont = document.getElementById("englishFont");

englishFont.addEventListener("click", toggleLanguage);
englishFont.addEventListener("click", removeClass);