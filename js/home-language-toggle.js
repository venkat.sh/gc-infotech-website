var defaultLanguage = 'english';
var websiteContent = {
  "english": {
    "topBar": {
      "feedback": 'Feedback',
      "complaintNumber": 'COMPLAINT NUMBER - 1913',
      "siteMap": 'Site map',
      "login": 'Login'
    },
    "banner": {
      "newsletter": 'GCC Newsletter Issue 6 Released'
    },
    "navMenu": {
      "home":'Home',
      "aboutGcc":'About GCC',
      "whoIsWho":'Who is Who',
      "council":'Council',
      "tender":'Tender',
      "departments": 'Departments',
      "services": 'Online Services'
    },
    "navTitle": {
      "Covid19":'COVID 19',
      "onlineServices":'ONLINE SERVICES',
      "onlinePayment":'ONLINE PAYMENTS',
      "departments":'DEPARTMENTS',
      "medicalServies":'MEDICAL SERVICES',
      "locationServices":'LOCATION SERVICES',
      "schemes":'SCHEMES'
    },
    "whatsNewTitle": {
      "whatsNew":'WHATS NEW !',
      "line1":'Southern Railway has revised the timings of two pairs of special train',
      "line2":'Trees uprooted in Cyclone Nivar in Chennai to be converted into useful products',
      "line3":'Southern Railway has revised the timings of two pairs of special train',
      "line4":'Trees uprooted in Cyclone Nivar in Chennai to be converted into useful products',
      "line5":'Southern Railway has revised the timings of two pairs of special train',
      "line6":'Trees uprooted in Cyclone Nivar in Chennai to be converted into useful products'
    },
    "quickLinkTitle": {
      "quickLink":'QUICK LINKS',
      "birthCertificate":'Birth Certificate',
      "deathCertificate":'Death Certificate',
      "propertyTax":'Property Tax',
      "townBuilding":'Town Building',
      "swatchBharat":'Swatch Bharat ',
      "volunteerRegistration":'Volunteer Registration ',
      "streetVendorDetails":'Street Vendor Details',
      "amurtReforms":'Amurt Reforms',
      "sLBandBalanceSheets":'SLB and Balance Sheets ',
      "righttoInformationAct":'Right to Information Act',
      "onlinePublicGrievance":'Online Public Grievance',
      "disasterManagementPlan":'Disaster Management Plan'
    },
    "galleryTitle": {
      "gallery":'Honourable Minister for Municipal Administration, Rural Development and Implementation of Special Programme Mr. S.P Velumani launched a Mobile Command and Control Centre in November 2020 to monitor waterlogging using flood sensors across the city in a real-time basis.'
    },
    "footerTitle": {
      "home":'Home',
      "aboutGCC":'About GCC',
      "departments":'Departments',
      "complaints":'Complaints',
      "weeklyBulletin":'Weekly Bulletin',
      "licensedSurveyors":'Licensed Surveyors',
      "architectlist":'Architect list',
      "siteMap":'Site Map',
      "law":'"The Bribery is against the Law. The Complaints about corruption may be sent to the Directorate of vigilance and Anti-Corruption, Chennai-16"',
      "website":'Website: www.dvac.tn.gov.in',
      "phonenumber":'Phone No. 22310989 / 22321090 / 223211085'
    },
    "DepartmentTitle":{
      "title":'DEPARTMENTS'
    }
  },
  "tamil": {
    "topBar": {
      "feedback": 'பின்னூட்டம்',
      "complaintNumber": 'புகார் எண் – 1913',
      "siteMap": 'தள வரைபடம்',
      "login": 'உள்நுழைய'
    },
    "banner": {
      "newsletter": 'மாநகராட்சியின் 6-வது செய்திமடல் வெளியீடு'
    },
    "navMenu": {
      "home": 'முகப்பு',
      "aboutGcc": 'சென்னை மாநகராட்சியைப் பற்றி',
      "whoIsWho": 'நிர்வாகம்',
      "council": 'மன்றம்',
      "tender": 'ஒப்பந்தப்புள்ளிகள்',
      "departments": 'துறைகள்',
      "services": 'இணையதள சேவைகள்'
    },
    "navTitle": {
      "Covid19":'கொரோனா நுண்கிருமி தொற்று',
      "onlineServices":'இணையதள சேவைகள்',
      "onlinePayment":'ஆன்லைன் கட்டணம்',
      "departments":'துறைகள்',
      "medicalServies":'மருத்துவ சேவைகள்',
      "locationServices":'முகவரி',
      "schemes":'திட்டங்கள்'
    },
    "whatsNewTitle": {
      "whatsNew":'தற்போதைய அறிவிப்புகள் !',
      "line1":'தெற்கு ரயில்வே இரண்டு சிறப்பு ரயில்களின் நேரத்தை திருத்தியுள்ளது',
      "line2":'சென்னையில் நிவார் சூறாவளியில் பிடுங்கப்பட்ட மரங்கள் பயனுள்ள தயாரிப்புகளாக மாற்றப்படும்',
      "line3":'தெற்கு ரயில்வே இரண்டு சிறப்பு ரயில்களின் நேரத்தை திருத்தியுள்ளது',
      "line4":'சென்னையில் நிவார் சூறாவளியில் பிடுங்கப்பட்ட மரங்கள் பயனுள்ள தயாரிப்புகளாக மாற்றப்படும்',
      "line5":'தெற்கு ரயில்வே இரண்டு சிறப்பு ரயில்களின் நேரத்தை திருத்தியுள்ளது',
      "line6":'தெற்கு ரயில்வே இரண்டு சிறப்பு ரயில்களின் நேரத்தை திருத்தியுள்ளது'
    },
    "quickLinkTitle": {
      "quickLink":'விரைவான இணைப்பு',
      "birthCertificate":'பிறப்பு சான்றிதழ்',
      "deathCertificate":'இறப்பு சான்றிதழ்',
      "propertyTax":'சொத்து வரி',
      "townBuilding":'நகரமைப்பு திட்டம்',
      "swatchBharat":'தூய்மை இந்தியா திட்டம்',
      "volunteerRegistration":'தன்னார்வலர்கள் பதிவீடு',
      "streetVendorDetails":'சாலையோர வியாபாரிகள் விவரம்',
      "amurtReforms":'அம்ருத் திட்டம்',
      "sLBandBalanceSheets":'SLB மற்றும் இருப்புநிலைகள்',
      "righttoInformationAct":'தகவல் உரிமைச் சட்டம்',
      "onlinePublicGrievance":'இணையதள புகார் சேவைகள்',
      "disasterManagementPlan":'பேரிடர் மேலாண்மை திட்டம்'
    },
    "galleryTitle": {
      "gallery":'மாண்புமிகு நகராட்சி நிர்வாகம், ஊரக வளர்ச்சி மற்றும் சிறப்பு திட்டங்கள் செயலாக்கத்துறை அமைச்சர் திரு. எஸ். பி. வேலுமணி அவர்கள், கடந்த 2020 நவம்பர் மாதத்தில் ஒருங்கிணைந்த நடமாடும் கட்டளை & கட்டுப்பாட்டு மைய வாகனத்தை தொடங்கி வைத்தார்.'
    },
    "footerTitle": {
      "home":'முகப்பு',
      "aboutGCC":'சென்னை மாநகராட்சியைப் பற்றி',
      "departments":'துறைகள்',
      "complaints":'புகார்கள்',
      "weeklyBulletin":'வாராந்திர அறிவிப்புகள்',
      "licensedSurveyors":'உரிமம் பெற்ற நில அளவர்கள்',
      "architectlist":'கட்டிடக்கலை நிபுணர்கள் பட்டியல்',
      "siteMap":'தள வரைபடம்',
      "law":'“லஞ்சம் சட்டத்திற்கு எதிரானது. ஊழல் தொடர்பான புகார்கள் சென்னை - 16 கண்காணிப்பு மற்றும் ஊழல் தடுப்பு இயக்குநரகத்திற்கு அனுப்பப்படலாம்."',
      "website":'இணையதளம்: www.dvac.tn.gov.in',
      "phonenumber":'தொலைபேசி எண்கள் - 22310989 / 22321090 / 223211085'
    },
    "DepartmentTitle":{
      "title":'துறைகள்'
    },
  }
};

var contentKeys = [
  "topBar-feedback",
  "topBar-complaintNumber",
  "topBar-siteMap",
  "topBar-login",
  "banner-newsletter",
  "navMenu-home",
  "navMenu-aboutGcc",
  "navMenu-whoIsWho",
  "navMenu-council",
  "navMenu-tender",
  "navMenu-departments",
  "navMenu-services",
  "navTitle-Covid19",
  "navTitle-onlineServices",
  "navTitle-onlinePayment",
  "navTitle-departments",
  "navTitle-medicalServies",
  "navTitle-locationServices",
  "navTitle-schemes",
  "whatsNewTitle-whatsNew",
  "whatsNewTitle-line1",
  "whatsNewTitle-line2",
  "whatsNewTitle-line3",
  "whatsNewTitle-line4",
  "whatsNewTitle-line5",
  "whatsNewTitle-line6",
  "quickLinkTitle-quickLink",
  "quickLinkTitle-birthCertificate",
  "quickLinkTitle-deathCertificate",
  "quickLinkTitle-propertyTax",
  "quickLinkTitle-townBuilding",
  "quickLinkTitle-swatchBharat",
  "quickLinkTitle-volunteerRegistration",
  "quickLinkTitle-streetVendorDetails",
  "quickLinkTitle-amurtReforms",
  "quickLinkTitle-sLBandBalanceSheets",
  "quickLinkTitle-righttoInformationAct",
  "quickLinkTitle-onlinePublicGrievance",
  "quickLinkTitle-disasterManagementPlan",
  "quickLinkTitle-amurtReforms",
  "galleryTitle-gallery",
  "footerTitle-home",
  "footerTitle-aboutGCC",
  "footerTitle-departments",
  "footerTitle-complaints",
  "footerTitle-weeklyBulletin",
  "footerTitle-licensedSurveyors",
  "footerTitle-architectlist",
  "footerTitle-siteMap",
  "footerTitle-law",
  "footerTitle-website",
  "footerTitle-phonenumber",
  "DepartmentTitle-title"
]