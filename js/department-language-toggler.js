var defaultLanguage = 'english';
var websiteContent = {
  "english": {
    "topBar": {
      "feedback": 'Feedback',
      "complaintNumber": 'COMPLAINT NUMBER - 1913',
      "siteMap": 'Site map',
      "login": 'Login'
    },
    "navMenu": {
      "home":'Home',
      "aboutGcc":'About GCC',
      "whoIsWho":'Who is Who',
      "council":'Council',
      "tender":'Tender',
      "departments": 'Departments',
      "services": 'Online Services'
    },
    "DepartmentTitle":{
      "title":'DEPARTMENTS'
    },
    "breadCrumb":{
      "departments":'Departments'
    },
    "departmentTile":{
      "health":'HEALTH',
      "revenue":'REVENUE',
      "education":'EDUCATION',
      "zonal":'ZONAL DETAILS',
      "townPlanning":'TOWN PLANNING',
      "electrical":'ELECTRICAL',
      "roads":'ROADS',
      "bridges":'BRIDGES',
      "parks":'PARKS',
      "building":'BUILDING',
      "solid":'SOLID WASTE MANAGEMENT',
      "strom":'STORM WATER DRAIN',
      "mechanical":'MECHANICAL ENGINEERING',
      "specialProject":'SPECIAL PROJECT',
      "finance":'FINANCE',
      "landEstate":'LAND & ESTATES',
      "legalCell":'LEGAL CELL'
    },
    "footerTitle": {
      "home":'Home',
      "aboutGCC":'About GCC',
      "departments":'Departments',
      "complaints":'Complaints',
      "weeklyBulletin":'Weekly Bulletin',
      "licensedSurveyors":'Licensed Surveyors',
      "architectlist":'Architect list',
      "siteMap":'Site Map',
      "law":'"The Bribery is against the Law. The Complaints about corruption may be sent to the Directorate of vigilance and Anti-Corruption, Chennai-16"',
      "website":'Website: www.dvac.tn.gov.in',
      "phonenumber":'Phone No. 22310989 / 22321090 / 223211085'
    }
  },
  "tamil": {
    "topBar": {
      "feedback": 'பின்னூட்டம்',
      "complaintNumber": 'புகார் எண் – 1913',
      "siteMap": 'தள வரைபடம்',
      "login": 'உள்நுழைய'
    },
    "navMenu": {
      "home": 'முகப்பு',
      "aboutGcc": 'சென்னை மாநகராட்சியைப் பற்றி',
      "whoIsWho": 'நிர்வாகம்',
      "council": 'மன்றம்',
      "tender": 'ஒப்பந்தப்புள்ளிகள்',
      "departments": 'துறைகள்',
      "services": 'இணையதள சேவைகள்'
    },
    "DepartmentTitle":{
      "title":'துறைகள்'
    },
    "breadCrumb":{
      "departments":'துறைகள்'
    },
    "departmentTile":{
      "health":'சுகாதாரம்',
      "revenue":'வருவாய்',
      "education":'கல்வி',
      "zonal":'மண்டல விவரம்',
      "townPlanning":'நகரமைப்பு பிரிவு',
      "electrical":'மின் துறை',
      "roads":'சாலை',
      "bridges":'பாலங்கள்',
      "parks":'பூங்கா',
      "building":'கட்டிடம்',
      "solid":'திடக்கழிவு மேலாண்மை',
      "strom":'மழைநீர் வடிகால்வாய்',
      "mechanical":'இயந்திரப் பொறியியல் துறை',
      "specialProject":'சிறப்பு திட்டங்கள் துறை',
      "finance":'நிதித்துறை',
      "landEstate":'நிலம் மற்றும் உடைமைத் துறை',
      "legalCell":'சட்டக்குழுமம்'
    },
    "footerTitle": {
      "home":'முகப்பு',
      "aboutGCC":'சென்னை மாநகராட்சியைப் பற்றி',
      "departments":'துறைகள்',
      "complaints":'புகார்கள்',
      "weeklyBulletin":'வாராந்திர அறிவிப்புகள்',
      "licensedSurveyors":'உரிமம் பெற்ற நில அளவர்கள்',
      "architectlist":'கட்டிடக்கலை நிபுணர்கள் பட்டியல்',
      "siteMap":'தள வரைபடம்',
      "law":'“லஞ்சம் சட்டத்திற்கு எதிரானது. ஊழல் தொடர்பான புகார்கள் சென்னை - 16 கண்காணிப்பு மற்றும் ஊழல் தடுப்பு இயக்குநரகத்திற்கு அனுப்பப்படலாம்."',
      "website":'இணையதளம்: www.dvac.tn.gov.in',
      "phonenumber":'தொலைபேசி எண்கள் - 22310989 / 22321090 / 223211085'
    }
  }
};

var contentKeys = [
  "topBar-feedback",
  "topBar-complaintNumber",
  "topBar-siteMap",
  "topBar-login",
  "navMenu-home",
  "navMenu-aboutGcc",
  "navMenu-whoIsWho",
  "navMenu-council",
  "navMenu-tender",
  "navMenu-departments",
  "navMenu-services",
  "DepartmentTitle-title",
  "breadCrumb-departments",
  "departmentTile-health",
  "departmentTile-revenue",
  "departmentTile-education",
  "departmentTile-zonal",
  "departmentTile-townPlanning",
  "departmentTile-electrical",
  "departmentTile-roads",
  "departmentTile-bridges",
  "departmentTile-parks",
  "departmentTile-building",
  "departmentTile-solid",
  "departmentTile-strom",
  "departmentTile-mechanical",
  "departmentTile-specialProject",
  "departmentTile-finance",
  "departmentTile-landEstate",
  "departmentTile-legalCell",
  "footerTitle-home",
  "footerTitle-aboutGCC",
  "footerTitle-departments",
  "footerTitle-complaints",
  "footerTitle-weeklyBulletin",
  "footerTitle-licensedSurveyors",
  "footerTitle-architectlist",
  "footerTitle-siteMap",
  "footerTitle-law",
  "footerTitle-website",
  "footerTitle-phonenumber"
]







