let getContent = (path, obj) => {
  return path.split('-').reduce(function (prev, curr) {
    //console.log('prev-->',prev)
    //console.log('curr-->',curr)
    return prev ? prev[curr] : null
  }, obj)
}


let applyContentFor = (languageName, contentPath) => {
  let element = document.getElementById(contentPath);
  if (element) {
    element.innerText = getContent(contentPath, websiteContent[languageName]);
  }

  // console.log('element---->',element)
  // if((element != "" ) && (element != null )){
  //   element.innerText = getContent(contentPath, websiteContent[languageName]);
  //   console.log('intter------>',element.innerText)
  // } else {
  //   console.log('Not available ->', contentPath);
  // }
}

let applyContent = (languageName) => {
  contentKeys.forEach((contentKey, index) => {
    applyContentFor(languageName, contentKey);

  });
}

let currentLanguage = defaultLanguage;
applyContent(currentLanguage);

function toggleLanguage(language) {
  currentLanguage = currentLanguage == 'english' ? 'tamil' : 'english';
   $("body").removeClass(language);
  //  $("body").removeAttr('class');
  if (language == 'tamil') {
    $("body").addClass(language);
  }

  applyContent(language);
}


$(document).ready(function () {

  $("#toggleLanguage").click(function () {

    var currentLanguage = Cookies.get('language');
    if (currentLanguage == 'english') {
      Cookies.set('language', 'tamil');

      $(this).text('English');
      toggleLanguage('tamil');
      $("body").addClass("tamil");
      console.log("3");
    } else if (currentLanguage == 'tamil') {
      Cookies.set('language', 'english');
      $(this).text('தமிழ்');
      toggleLanguage('english');
      $("body").removeClass("tamil");
      $("body").addClass("english");
      console.log("4");
    }


  });

});

$(window).on('load', function () {

  let language = Cookies.get('language');

  if (typeof language == 'undefined') {
    Cookies.set('language', 'english');
    $("body").addClass("english");
  } else {
    if (language == 'english') {
      $("#toggleLanguage").text('தமிழ்');
      toggleLanguage('english');
      console.log("1");
    } else if (language == 'tamil') {
      $("#toggleLanguage").text('English');
      toggleLanguage('tamil');
      console.log("2");
    }
  }
  // load font size
  let fontsize = Cookies.get('fontsize');

  if (typeof fontsize == 'undefined') {
  } else {
   $("."+fontsize).trigger('click');
  }
});








